# README #
### What is this repository for? ###

Репозиторий для приёма и проверки лабораторных группы ИСМ по дисциплине ВП.

### How do I get set up? ###

Все лабораторные должны быть оформлены как **maven-проекты**, тип проекта выбирается в зависимости от нужд лабораторной. По-умолчанию - **Java Application**.

В качестве примеров оформления в репозитории уже присутствуют 2 проекта: **Filgus** и **Zzz**.

Все лабораторные должны быть отформатированы согласно Java coding conventions, большую часть из которых по-умолчанию предоставляет Netbeans(а больше и не особо надо). За неотформатированный код оценка будет снижаться на 1 балл.

### Contribution guidelines ###

* Для сдачи лабораторной необходимо сделать **fork** и **branch** от основной ветки **master**. Название branch'а формируется как: **%NAME%_LAB_N**, где **%NAME%** - фамилия студента, а **N** - номер лабораторной (к примеру, FILGUS_LAB_1).
* Корневой пакет лабораторной оформляется по шаблону:
ru.mirea.iis.vp.**username**.labs.first, где **username** - фамилия студента в *нижнем* регистре. Для второй и третьей лабораторной конечный пакет будет не "**first**", а "**second**" и "**third**", соотвественно.
* После завершения работы над лабораторной необходимо отправить все изменения в свой branch и оформить **pull-request** в **master** основного репозитория, прохождение которого будет означать завершение процесса сдачи лабораторной и присвоения заслуженной оценки.
* Репозиторий должен быть публичным, либо у преподавателя должно быть разрешение на работу с fork'ом репозитория.
* Оцениваться будет тот код, который был отправлен в pull-request'е. Любые недочёты, ошибки, помарки и прочие несовершенства будут влиять на оценку в сторону её понижения.
* При возникновении вопросов в реализации лабораторной следует сделать **push** в собственный репозиторий, после чего написать преподавателю на почту вопрос с ссылкой на репозиторий.